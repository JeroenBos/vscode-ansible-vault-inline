# Change Log

All notable changes to the `ansible-vault-inline` extension will be documented in this file.

## [Unreleased]
- Support using `ansible` from WSL
- Support untitled files [#5](https://gitlab.com/wolfmah/vscode-ansible-vault-inline/-/issues/5)]

## [0.4.0] - 2020-07-12
### Changed
- Support multiple vault-id [[MR!3](https://gitlab.com/wolfmah/vscode-ansible-vault-inline/-/merge_requests/3)]

## [0.3.0] - 2019-11-12
### Changed
- Added '!vault |' when encrypting
- Kept the indentation of the inline string when encrypting

## [0.2.0] - 2019-10-22
### Changed
- Better documentation
- Displayed command name in contextual menu
### Fix
- Command show up in command palette

## [0.1.0] - 2019-10-21
### Added
- Initial release
